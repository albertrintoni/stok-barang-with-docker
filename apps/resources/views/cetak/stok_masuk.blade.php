<!DOCTYPE html>
<html>
<head>
	<title>Laporan Stok Masuk</title>
	<style type="text/css">
			table {
			  border-collapse: collapse;
			}

			table, th, td {
			  border: 1px solid black;
			}
			.font {
				font-family: sans-serif;
			}
	</style>
</head>
<body>
<center>
	<h2 class="font" style="font-size: 18px;">Laporan Stok Masuk</h2>
	<h5 class="font" style="font-size: 14px;">Tanggal : {{ $tanggalSekarang}} </h5>
</center>
<table>
		<thead class="font" style="font-size: 12px;">
			<tr>
				<th>No.</th>
				<th>Nama Barang</th>
				<th>Vendor</th>
				<th>Tanggal</th>
				<th>Jumlah</th>
			</tr>
		</thead>
		@foreach($stok_masuk as $sm)
		<tbody class="font" style="font-size: 12px;">
			<tr>
				<td width="30">
					<center>{{ $nomer++}}</center>
				</td>
				<td width="150">{{ $sm->nama_barang}} </td>
				<td width="150">{{ $sm->vendor}} </td>
				<td width="150">{{ $sm->created_at}} </td>
				<td width="50">
					<center>{{ $sm->jumlah}}</center>
				</td>
			</tr>
		</tbody>
		@endforeach
		<tr>
			<th colspan="4" style="font-size: 12px;">
				<center>Total</center>
			</th>
			<th style="font-size: 12px;">{{ $total }} </th>
		</tr>
	</table>
</body>
</html>
