@extends('../template/template')

@section('judul','Dashboard Administrator')
@section('sub-judul','Dashboard')

@section('konten')
<!-- Content Row -->
<div class="row">
	<!-- Earnings (Monthly) Card Example -->
	<div class="col-xl-4 col-md-6 mb-4">
	  <div class="card border-left-primary shadow h-100 py-2">
	    <div class="card-body">
	      <div class="row no-gutters align-items-center">
	        <div class="col mr-2">
	          <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Stok Masuk</div>
	          <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $totalStokMasuk}} </div>
	        </div>
	        <div class="col-auto">
	          <i class="fas fa-arrow-down fa-2x text-primary"></i>
	        </div>
	      </div>
	    </div>
	  </div>
	</div>

	<!-- Earnings (Monthly) Card Example -->
	<div class="col-xl-4 col-md-6 mb-4">
	  <div class="card border-left-danger shadow h-100 py-2">
	    <div class="card-body">
	      <div class="row no-gutters align-items-center">
	        <div class="col mr-2">
	          <div class="text-xs font-weight-bold text-danger text-uppercase mb-1">Stok Keluar</div>
	          <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $totalStokKeluar }}</div>
	        </div>
	        <div class="col-auto">
	          <i class="fas fa-arrow-up fa-2x text-danger"></i>
	        </div>
	      </div>
	    </div>
	  </div>
	</div>

	<!-- Pending Requests Card Example -->
	<div class="col-xl-4 col-md-6 mb-4">
	  <div class="card border-left-warning shadow h-100 py-2">
	    <div class="card-body">
	      <div class="row no-gutters align-items-center">
	        <div class="col mr-2">
	          <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">Total Stok</div>
	          <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $totalStok}} </div>
	        </div>
	        <div class="col-auto">
	          <i class="fas fa-check fa-2x text-warning"></i>
	        </div>
	      </div>
	    </div>
	  </div>
	</div>
</div>

<!-- Chart -->
<div class="row">

	<!-- Illustrations -->
	<div class="col-xl-8 col-lg-5">
		<div class="card shadow mb-4">
	    <div class="card-header py-3">
	      <h6 class="m-0 font-weight-bold text-primary">E-Stok Barang</h6>
	    </div>
	    <div class="card-body">
	      <div class="text-center">
	        <img class="img-fluid px-3 px-sm-4 mt-3 mb-4" style="width: 25rem;" src="{{ asset('assets/img/undraw_posting_photo.svg')}}" alt="">
	      </div>
	      <p>Aplikasi stok barang berbasis web ini dapat mengatus dan mengelola data stok barang masuk dan keluar. <br>
	      Juga terdapat fitur membuat laporan dan mendapatkan notifikasi jika barang tersisa 1 lagi.</p>
	    </div>
	  </div>
	</div>

	<!-- Pie Chart -->
    <div class="col-xl-4 col-lg-5">
      <div class="card shadow mb-4">
        <!-- Card Header - Dropdown -->
        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
          <h6 class="m-0 font-weight-bold text-primary">Info Chart Stok Barang</h6>
        </div>
        <!-- Card Body -->
        <div class="card-body">
          <div class="chart-pie pt-4 pb-2">
            <canvas id="chartStok"></canvas>
          </div>
          <div class="mt-4 text-center small">
            <span class="mr-2">
              <i class="fas fa-circle text-primary"></i> Stok Masuk
            </span>
            <span class="mr-2">
              <i class="fas fa-circle text-danger"></i> Stok Keluar
            </span>
            <span class="mr-2">
              <i class="fas fa-circle text-warning"></i> Total Stok
            </span>
          </div>
        </div>
      </div>
    </div>
</div>
<!-- ./Chart -->

@endsection

@section('js')

<script type="text/javascript">
	// Set new default font family and font color to mimic Bootstrap's default styling
Chart.defaults.global.defaultFontFamily = 'Nunito', '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
Chart.defaults.global.defaultFontColor = '#858796';

// Pie Chart Example
var ctx = document.getElementById("chartStok");
var chartStok = new Chart(ctx, {
  type: 'doughnut',
  data: {
    labels: ["Stok Masuk", "Stok Keluar", "Total Stok"],
    datasets: [{
      data: [<?= $totalStokMasuk?> , <?= $totalStokKeluar ?> , <?= $totalStok ?>],
      backgroundColor: ['#3498db', '#e74c3c', '#f1c40f'],
      hoverBackgroundColor: ['#2980b9', '#c0392b', '#f39c12'],
      hoverBorderColor: "rgba(234, 236, 244, 1)",
    }],
  },
  options: {
    maintainAspectRatio: false,
    tooltips: {
      backgroundColor: "rgb(255,255,255)",
      bodyFontColor: "#858796",
      borderColor: '#dddfeb',
      borderWidth: 1,
      xPadding: 15,
      yPadding: 15,
      displayColors: false,
      caretPadding: 10,
    },
    legend: {
      display: false
    },
    cutoutPercentage: 80,
  },
});

</script>
@endsection