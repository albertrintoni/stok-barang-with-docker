<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Total_Stoks;
use App\Stok_Masuks;
use App\Stok_Keluars;
use PDF;

class Laporan_Stok extends Controller
{
    public function stok_masuk(Request $request){
    	// Halaman Laporan Stok Masuk

        // Stok Sisa 1 Barang
        $sisaStok = Total_Stoks::where('jumlah',1)->sum('jumlah');
        // Deskripsi Sisa Stok 1 Barang
        $deskripsiSisaStok = Total_Stoks::all()->where('jumlah',1);

        $nomer = 1;

        $stok_masuk = Stok_Masuks::whereBetween('created_at', [$request->mulai, $request->akhir])->get();
        // Menjumlahkan semua stok dari hasil cari
        $total = $stok_masuk->sum('jumlah');

        if ($request->cetak) {
            $tanggalSekarang = \Carbon\Carbon::now();
            $pdf = PDF::loadview('cetak.stok_masuk',[
                'stok_masuk' => $stok_masuk,
                'nomer' => $nomer,
                'total' => $total,
                'tanggalSekarang' => $tanggalSekarang]);
            return $pdf->stream('laporan-stok-masuk.pdf');
        }
        else {
            $total = $stok_masuk->sum('jumlah');
            return view('dashboard.laporan_stok_masuk',[
            'sisaStok' => $sisaStok,
            'deskripsiSisaStok' => $deskripsiSisaStok,
            'stok_masuk' => $stok_masuk,
            'total' => $total,
            'nomer' => $nomer
        ]);
        }
    }

    public function stok_keluar(Request $request){
        // Halaman Laporan Stok Masuk

        // Stok Sisa 1 Barang
        $sisaStok = Total_Stoks::where('jumlah',1)->sum('jumlah');
        // Deskripsi Sisa Stok 1 Barang
        $deskripsiSisaStok = Total_Stoks::all()->where('jumlah',1);

        $nomer = 1;

        $stokKeluar = Stok_Keluars::whereBetween('created_at', [$request->mulai, $request->akhir])->get();
        // Menjumlahkan semua stok dari hasil cari
        $total = $stokKeluar->sum('jumlah');

        if ($request->cetak) {
            $tanggalSekarang = \Carbon\Carbon::now();
            $pdf = PDF::loadview('cetak.stok_keluar',[
                'stokKeluar' => $stokKeluar,
                'nomer' => $nomer,
                'total' => $total,
                'tanggalSekarang' => $tanggalSekarang]);
            return $pdf->stream('laporan-stok-keluar.pdf');
        }
        else {
            return view('dashboard.laporan_stok_keluar',[
                'sisaStok' => $sisaStok,
                'deskripsiSisaStok' => $deskripsiSisaStok,
                'stokKeluar' => $stokKeluar,
                'total' => $total,
                'nomer' => $nomer
            ]);
        }
    }
    public function total_stok(Request $request){
        // Halaman Laporan Stok Masuk

        // Stok Sisa 1 Barang
        $sisaStok = Total_Stoks::where('jumlah',1)->sum('jumlah');
        // Deskripsi Sisa Stok 1 Barang
        $deskripsiSisaStok = Total_Stoks::all()->where('jumlah',1);

        $nomer = 1;

        $totalStok = Total_Stoks::whereBetween('created_at', [$request->mulai, $request->akhir])->get();
        // Menjumlahkan semua stok dari hasil cari
        $total = $totalStok->sum('jumlah');

        if ($request->cetak) {
            $tanggalSekarang = \Carbon\Carbon::now();
            $pdf = PDF::loadview('cetak.total_stok',[
                'totalStok' => $totalStok,
                'nomer' => $nomer,
                'total' => $total,
                'tanggalSekarang' => $tanggalSekarang]);
            return $pdf->stream('laporan-stok-masuk.pdf');
        }
        else {
            return view('dashboard.laporan_total_stok',[
            'sisaStok' => $sisaStok,
            'deskripsiSisaStok' => $deskripsiSisaStok,
            'totalStok' => $totalStok,
            'total' => $total,
            'nomer' => $nomer
        ]);
        }
        
    }
}
