<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Total_Stoks extends Model
{
    // Mengambil data dari tabel total_stok
    protected $table = 'total_stok';

    protected $fillable = ['nama_barang','vendor','jumlah'];
}
