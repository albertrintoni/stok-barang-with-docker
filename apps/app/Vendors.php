<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vendors extends Model
{
    // Mengambil dari tabel vendor
    protected $table = 'vendor';

    protected $fillable = ['nama_vendor'];
}
