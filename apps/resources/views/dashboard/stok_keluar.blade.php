@extends('../template/template')

@section('judul','Stok Keluar')

@section('sub-judul','Stok Keluar')

@section('konten')
<!-- DataTales Example -->
<div class="card shadow mb-4">
		<div class="card-header py-3">
			<div class="row">
				<div class="col">
		  			<h6 class="m-0 font-weight-bold text-primary" style="padding-top: 5px;">Tabel Stok Keluar</h6>
				</div>
				<div class="col">			
		 			<button class="btn btn-primary btn-sm float-right" data-toggle="modal" data-target="#tambahStokKeluar">Tambah Stok Keluar  
		 				<i class="fa fa-plus"></i>
		 			</button>
				</div>
			</div>
		</div>
		@if ($errors->any())
		    <div class="container">
		        <div class="alert alert-danger">
		            <ul>
		                @foreach ($errors->all() as $error)
		                    <li>{{ $error }}</li>
		                @endforeach
		            </ul>
		        </div>
		    </div>
		@elseif (session('berhasil'))
			<div class="alert alert-success">
				{{ session('berhasil')}}
			</div>
		@elseif (session('hapus'))
			<div class="alert alert-danger">
				{{ session('hapus')}}
			</div>
		@elseif(session('ubah'))
			<div class="alert alert-success">
				{{ session('ubah')}}
			</div>
		@elseif(session('tidakvalid'))
			<div class="alert alert-danger">
				{{ session('tidakvalid')}}
			</div>
		@endif
		<div class="card-body">
		  <div class="table-responsive">
		    <table class="table table-bordered" width="100%" cellspacing="0">
		      <thead>
		        <tr>
		          <th>No.</th>
		          <th>Nama Barang</th>
		          <th>Vendor</th>
		          <th>Jumlah</th>
		          <th>Tanggal Keluar</th>
		          <th>Opsi</th>
		        </tr>
		      </thead>
		      <tbody>
		      	@foreach($stok_keluar as $sk)
		        <tr>
		          <td>{{ $nomer++ }} </td>
		          <td>{{ $sk->nama_barang}}</td>
		          <td>{{ $sk->vendor}}</td>
		          <td>{{ $sk->jumlah}}</td>
		          <td>{{ $sk->created_at}}</td>
		          <td>
		          	<a href="{{ url('/stok_keluar/hapus')}}/{{$sk->id}}" class="text-danger">
		          		<i class="fa fa-trash"></i>
		          		Hapus
		          	</a>
		          	<button data-target="#show-{{ $sk->id}}" data-toggle="modal" style="background: transparent; border: none;" class="text-primary">
		          		<i class="fa fa-pen"></i>
		          		Edit
		          	</button>
		          </td>
		        </tr>
		        @endforeach
		      </tbody>
		    </table>
		  </div>
		</div>
</div>

<!-- Modal Tambah Vendor-->
<div class="modal fade" id="tambahStokKeluar" tabindex="-1" role="dialog" aria-labelledby="tambahStokKeluar" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Tambah Data Stok Keluar</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
	    <form method="post" action="{{ url('/stok_keluar')}}">
	    	{{ csrf_field()}}
	    	<label>Nama Barang : </label>
	    	<select name="nama_barang" class="form-control">
	    		@foreach($barang as $b)
	    		<option value="{{$b->nama_barang}}">{{$b->nama_barang}}</option>
	    		@endforeach
	    	</select>
	    	<label>Dari Vendor : </label>
	    	<select name="vendor" class="form-control">
	    		@foreach($vendor as $v)
	    		<option value="{{$v->nama_vendor}}">{{$v->nama_vendor}}</option>
	    		@endforeach
	    	</select> 
	    	<label>Jumlah : </label>
	    	<input type="number" name="jumlah" class="form-control"> 
	  </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button class="btn btn-primary">Save changes</button>
      </div>
	    </form>
    </div>
  </div>
</div>
<!-- Modal Edit Barang-->
@foreach ($stok_keluar as $sk)
<div class="modal fade" id="show-{{$sk->id}}" tabindex="-1" role="dialog" aria-labelledby="tampilVendor" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Edit Data Stok Keluar</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form method="post" action="{{ url('/stok_keluar/update')}}/{{ $sk->id }}">
	      <div class="modal-body">
		    	{{ csrf_field()}}
		    	{{ method_field('PUT')}}
		    	<label>Nama Barang : </label>
		    	<select name="nama_barang" class="form-control">
		    		@foreach($barang as $b)
		    		<option value="{{$b->nama_barang}}">{{$b->nama_barang}}</option>
		    		@endforeach
		    	</select>
		    	<label>Dari Vendor : </label>
		    	<select name="vendor" class="form-control">
		    		@foreach($vendor as $v)
		    		<option value="{{$v->nama_vendor}}">{{$v->nama_vendor}}</option>
		    		@endforeach
		    	</select> 
		    	<label>Jumlah : </label>
		    	<input type="text" name="jumlah" class="form-control" value="{{ $sk->jumlah}}"> 
		  </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	        <button class="btn btn-primary">Save changes</button>
	      </div>
	  </form>
    </div>
  </div>
</div>
@endforeach

<div class="modal fade" id="tambahStokKeluar" tabindex="-1" role="dialog" aria-labelledby="tambahStokKeluar" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Tambah Data Stok Keluar</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
	    <form method="post" action="{{ url('/stok_keluar')}}">
	    	{{ csrf_field()}}
	    	<label>Nama Barang : </label>
	    	<select name="nama_barang" class="form-control">
	    		@foreach($barang as $b)
	    		<option value="{{$b->nama_barang}}">{{$b->nama_barang}}</option>
	    		@endforeach
	    	</select>
	    	<label>Dari Vendor : </label>
	    	<select name="vendor" class="form-control">
	    		@foreach($vendor as $v)
	    		<option value="{{$v->nama_vendor}}">{{$v->nama_vendor}}</option>
	    		@endforeach
	    	</select> 
	    	<label>Jumlah : </label>
	    	<input type="number" name="jumlah" class="form-control"> 
	  </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button class="btn btn-primary">Save changes</button>
      </div>
	    </form>
    </div>
  </div>
</div>
@endsection
