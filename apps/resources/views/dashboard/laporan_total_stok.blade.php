@extends('../template/template')

@section('judul','Laporan Total Stok')

@section('sub-judul','Laporan Total Stok')

@section('konten')

<div class="card">
	<div class="card-body">
		<form method="get" action="{{ url('/laporan-total-stok')}}">
			<div class="row">
				<div class="col">	
					<label>Mulai Tanggal :</label>
					<input type="date" name="mulai" class="form-control">
				</div>
				<div class="col">
					<label>Sampai Tanggal :</label>
					<input type="date" name="akhir" class="form-control">
				</div>
				<div class="col">
					<input type="submit" name="cari" value="Cari" class="btn btn-primary" style="margin-top: 8%;">
					<input type="submit" name="cetak" value="Cetak PDF" class="btn btn-warning" style="margin-top: 8%;">
				</div>
			</div>
		</form>
		<br>
		
	</div>
</div>
<br>
<div class="card">
	<div class="card-body">
		<br><br>
		<div class="table-responsive">
			<table class="table table-bordered">
				<thead>
					<th>No</th>
					<th>Nama Barang</th>
					<th>Vendor</th>
					<th>Tanggal</th>
					<th>Jumlah</th>
				</thead>
				@foreach($totalStok as $ts)
				<tbody>
					<td>{{ $nomer++}} </td>
					<td>{{ $ts->nama_barang}} </td>
					<td>{{ $ts->vendor}} </td>
					<td>{{ $ts->created_at}} </td>
					<td>{{ $ts->jumlah}} </td>
				</tbody>
				@endforeach
				<tr>
					<th colspan="4">
						<center>Total</center>
					</th>
					<th>{{ $total}} </th>
				</tr>
			</table>
		</div>
	</div>
</div>
@endsection