<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Vendors;
use App\Total_Stoks;

class Vendor extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Halaman Vendor

        $nomer = 1;
        $vendor = Vendors::all(); // Mengambil semua data dari tabel vendor
        // Stok Sisa 1 Barang
        $sisaStok = Total_Stoks::where('jumlah',1)->sum('jumlah');
        // Deskripsi Sisa Stok 1 Barang
        $deskripsiSisaStok = Total_Stoks::all()->where('jumlah',1);

        return view('dashboard.vendor',[
            'nomer' => $nomer,
            'vendor' => $vendor,
            'sisaStok' => $sisaStok,
            'deskripsiSisaStok' => $deskripsiSisaStok
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Proses Input Data Barang
        // 1. Validasi pada form input
        $validasi = $request->validate([
            'nama_vendor' => 'required|min:3'
        ]);

        if($validasi){
            // 2.Jika validasi sudah di lewati, maka masuk ke tabel barang
            Vendors::create(['nama_vendor' => $request->nama_vendor]);
            // 3. Dan akan di pindahkan ke halaman /barang
            return redirect()->back();
        }
        else {
            // 4.Jika validasi gagal, maka akan tetap di halaman /barang
            return redirect()->back()
            ->withErrors($validasi)
            ->withInput();
        }
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Proses Input Data Barang
        // 1. Validasi pada form input
        $validasi = $request->validate([
            'nama_vendor' => 'required|min:3'
        ]);

        if($validasi){
            // 2.Jika validasi sudah di lewati, maka masuk ke tabel barang
            $update = Vendors::find($id);
            $update->nama_vendor = $request->nama_vendor;
            $update->save();
            // 3. Dan akan di pindahkan ke halaman /barang
            return redirect()->back();
        }
        else {
            // 4.Jika validasi gagal, maka akan tetap di halaman /barang
            return redirect()->back()
            ->withErrors($validasi)
            ->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Menhapus data Vendor per ID
        $hapus = Vendors::find($id);
        $hapus->delete();
        // Setelah itu Pindah ke halaman awal
        return redirect()->back();
    }
}
