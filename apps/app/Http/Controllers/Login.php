<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class Login extends Controller
{
    // Halaman Login
    public function index(){
    	return view ('login');
    }

    public function proses(Request $request){

    	if (Auth::attempt($request->only('email','password'))) {
    		
    		return redirect('/dashboard');
    	}
    	else {
    		return redirect('/login');
    	}
    }

    public function logout(){
    	Auth::logout();
    	return redirect('/login');
    }
}
