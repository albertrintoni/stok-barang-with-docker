@extends('../template/template')

@section('judul','Vendor')

@section('sub-judul','Vendor')

@section('konten')
<!-- DataTales Example -->
<div class="card shadow mb-4">
		<div class="card-header py-3">
			<div class="row">
				<div class="col">
		  			<h6 class="m-0 font-weight-bold text-primary" style="padding-top: 5px;">Tabel Vendor</h6>
				</div>
				<div class="col">			
		 			<button class="btn btn-primary btn-sm float-right" data-toggle="modal" data-target="#tambahVendor">Tambah Vendor  
		 				<i class="fa fa-plus"></i>
		 			</button>
				</div>
			</div>
		</div>
		@if ($errors->any())
		    <div class="container">
		        <div class="alert alert-danger">
		            <ul>
		                @foreach ($errors->all() as $error)
		                    <li>{{ $error }}</li>
		                @endforeach
		            </ul>
		        </div>
		    </div>
		@endif
		<div class="card-body">
		  <div class="table-responsive">
		    <table class="table table-bordered" width="100%" cellspacing="0">
		      <thead>
		        <tr>
		          <th>No.</th>
		          <th>Nama Vendor</th>
		          <th>Opsi</th>
		        </tr>
		      </thead>
		      <tbody>
		      	@foreach($vendor as $v)
		        <tr>
		          <td>{{ $nomer++ }} </td>
		          <td>{{ $v->nama_vendor}} </td>
		          <td>
		          	<a href="{{ url('/vendor/hapus')}}/{{$v->id}}" class="text-danger">
		          		<i class="fa fa-trash"></i>
		          		Hapus
		          	</a>
		          	<button data-target="#show-{{ $v->id}}" data-toggle="modal" style="background: transparent; border: none;" class="text-primary">
		          		<i class="fa fa-pen"></i>
		          		Edit
		          	</button>
		          </td>
		        </tr>
		        @endforeach
		      </tbody>
		    </table>
		  </div>
		</div>
</div>

<!-- Modal Tambah Vendor-->
<div class="modal fade" id="tambahVendor" tabindex="-1" role="dialog" aria-labelledby="tambahVendor" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Tambah Vendor</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
	    <form method="post" action="{{ url('/vendor')}}">
	    	{{ csrf_field()}}
	    	<label>Nama Vendor : </label>
	    	<input type="text" name="nama_vendor" class="form-control">  
	  </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button class="btn btn-primary">Save changes</button>
      </div>
	    </form>
    </div>
  </div>
</div>
<!-- Modal Edit Barang-->
@foreach ($vendor as $v)
<div class="modal fade" id="show-{{$v->id}}" tabindex="-1" role="dialog" aria-labelledby="tampilVendor" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Edit Vendor</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form method="post" action="{{ url('/vendor/update')}}/{{ $v->id }}">
	      <div class="modal-body">
		    	{{ csrf_field()}}
		    	{{ method_field('PUT')}}
		    	<label>Nama Vendor : </label>
		    	<input type="text" name="nama_vendor" class="form-control" value="{{ $v->nama_vendor}}">  
		  </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	        <button class="btn btn-primary">Save changes</button>
	      </div>
	  </form>
    </div>
  </div>
</div>
@endforeach
@endsection