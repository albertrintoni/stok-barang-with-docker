<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Stok_Masuks;
use App\Stok_Keluars;
use App\Total_Stoks;

class Dashboard extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Halaman Dashboard

        // Total Stok Masuk
        $totalStokMasuk = Stok_Masuks::all()->sum('jumlah');
        // Total Stok Keluar
        $totalStokKeluar = Stok_Keluars::all()->sum('jumlah');
        // Stok Sisa 1 Barang
        $sisaStok = Total_Stoks::where('jumlah',1)->sum('jumlah');
        // Deskripsi Sisa Stok 1 Barang
        $deskripsiSisaStok = Total_Stoks::all()->where('jumlah',1);
        // Total Stok
        $totalStok = Total_Stoks::all()->sum('jumlah');

        return view('dashboard.index',[
            'totalStokMasuk' => $totalStokMasuk,
            'totalStokKeluar' => $totalStokKeluar,
            'totalStok' => $totalStok,
            'sisaStok' => $sisaStok,
            'deskripsiSisaStok' => $deskripsiSisaStok

        ]);
    }

}
