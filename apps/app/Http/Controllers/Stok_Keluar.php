<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Stok_Keluars;
use App\Vendors;
use App\Barangs;
use App\Stok_Masuks;
use App\Total_Stoks;
use Session;

class Stok_Keluar extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Halaman Stok Keluar

        $nomer = 1;
        $stok_keluar = Stok_Keluars::all(); // Mengambil semua data dari tabel stok_keluar
        $barang = Barangs::all(); // Mengambil semua data dari tabel barang
        $vendor = Vendors::all(); // Mengambil semua data dari tabel vendor
        // Stok Sisa 1 Barang
        $sisaStok = Total_Stoks::where('jumlah',1)->sum('jumlah');
        // Deskripsi Sisa Stok 1 Barang
        $deskripsiSisaStok = Total_Stoks::all()->where('jumlah',1);

        return view('dashboard.stok_keluar',[
            'nomer' => $nomer,
            'stok_keluar' => $stok_keluar,
            'barang' => $barang,
            'vendor' => $vendor,
            'sisaStok' => $sisaStok,
            'deskripsiSisaStok' => $deskripsiSisaStok
        ]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Proses Input Data Stok Keluar
        // 1. Validasi pada form input
        $validasi = $request->validate([
            'nama_barang' => 'required|min:3',
            'vendor' => 'required|min:3',
            'jumlah' => 'required|numeric'
        ]);

        if($validasi){

            // 2.Jika validasi sudah di lewati, maka masuk ke tabel stok_keluar
            $namaBarang = $request->nama_barang;
            $vendor = $request->vendor;
            $jumlah = $request->jumlah;
            $cari = Total_Stoks::all()->where('nama_barang',$namaBarang)->where('vendor',$vendor);

            if ($cari) {
                $update = Total_Stoks::where('nama_barang',$namaBarang)->where('vendor',$vendor)->first();
                if($update->jumlah >= $jumlah){
                    
                    $update->jumlah -= $jumlah;
                    $update->save();

                    Stok_Keluars::create([
                        'nama_barang' => $request->nama_barang,
                        'vendor' => $request->vendor,
                        'jumlah' => $request->jumlah
                    ]);
                    
                    return redirect()->back()->with('berhasil','Data Stok Keluar Berhasil Ditambahkan!');
                }
                else{
                    return redirect()->back()->with('tidakvalid','Input jumlah tidak boleh lebih besar dari jumlah sebelumnya!');
                }
                

                
            }
            else {
                return redirect()->back()
                ->withErrors($validasi)
                ->withInput();
            }

            // 3. Dan akan di pindahkan ke halaman /stok_keluar
            
        }
        else {
            // 4.Jika validasi gagal, maka akan tetap di halaman /stok_keluar
            return redirect()->back()
            ->withErrors($validasi)
            ->withInput();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Proses Update Data Stok Keluar
        // 1. Validasi pada form input
        $validasi = $request->validate([
            'nama_barang' => 'min:3',
            'vendor' => 'min:3',
            'jumlah' => 'numeric'
        ]);

        if($validasi){
            // 2.Jika validasi sudah di lewati, maka masuk ke tabel stok_keluar
            $update = Stok_Keluars::find($id);
            $update->nama_barang = $request->nama_barang;
            $update->vendor = $request->vendor;
            $update->jumlah = $request->jumlah;
            $update->save();
            // 3. Dan akan di pindahkan ke halaman /stok_keluar
            return redirect()->back()->with('ubah','Data Berhasil Diubah!');
        }
        else {
            // 4.Jika validasi gagal, maka akan tetap di halaman /stok_keluar
            return redirect()->back()
            ->withErrors($validasi)
            ->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Menghapus data Stok Keluar per ID
        $hapus = Stok_Keluars::find($id);
        $hapus->delete();

        // Setelah itu pindah ke halaman awal
        return redirect()->back()->with('hapus','Data Berhasil Dihapus!');
    }
}
