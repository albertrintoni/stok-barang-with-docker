<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stok_Keluars extends Model
{
    // Mengambild data dari tabel stok_keluar
    protected $table = 'stok_keluar';

    protected $fillable = ['nama_barang','vendor','jumlah'];
}
