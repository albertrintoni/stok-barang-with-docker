FROM php:7.3-apache
FROM phpmyadmin

RUN docker-php-ext-install pdo pdo_mysql && docker-php-ext-enable pdo pdo_mysql

EXPOSE 80
