<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stok_Masuks extends Model
{
    // Mengambil dari tabel stok_masuk
    protected $table = 'stok_masuk';

    protected $fillable = ['nama_barang','vendor','jumlah'];
}
