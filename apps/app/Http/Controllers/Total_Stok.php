<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Total_Stoks;

class Total_Stok extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Halaman Total Stok

        $nomer = 1;
        $totalStok = Total_Stoks::all();

        // Stok Sisa 1 Barang
        $sisaStok = Total_Stoks::where('jumlah',1)->sum('jumlah');
        // Deskripsi Sisa Stok 1 Barang
        $deskripsiSisaStok = Total_Stoks::all()->where('jumlah',1);

        return view('dashboard.total_stok',[
            'nomer' => $nomer,
            'totalStok' => $totalStok,
            'sisaStok' => $sisaStok,
            'deskripsiSisaStok' => $deskripsiSisaStok
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
