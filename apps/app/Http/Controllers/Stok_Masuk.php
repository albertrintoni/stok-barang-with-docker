<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Stok_Masuks;
use App\Barangs;
use App\Vendors;
use App\Total_Stoks;
use Sessions;

class Stok_Masuk extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Halaman Stok Masuk

        $nomer = 1;
        $stok_masuk = Stok_Masuks::all(); // Mengambil semua data dari tabel stok_masuk
        $barang = Barangs::all(); // Mengambil semua data dari tabel barang
        $vendor = Vendors::all(); // Mengambil semua data dari tabel vendor
        // Stok Sisa 1 Barang
        $sisaStok = Total_Stoks::where('jumlah',1)->sum('jumlah');
        // Deskripsi Sisa Stok 1 Barang
        $deskripsiSisaStok = Total_Stoks::all()->where('jumlah',1);

        return view('dashboard.stok_masuk',[
            'nomer' => $nomer,
            'stok_masuk' => $stok_masuk,
            'barang' => $barang,
            'vendor' => $vendor,
            'sisaStok' => $sisaStok,
            'deskripsiSisaStok' => $deskripsiSisaStok
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        // Proses Input Data Stok Masuk
        // 1. Validasi pada form input
        $validasi = $request->validate([
            'nama_barang' => 'required|min:3',
            'vendor' => 'required|min:3',
            'jumlah' => 'required|numeric'
        ]);

        if($validasi){
            // 2.Jika validasi sudah di lewati, maka masuk ke tabel stok_masuk dan total_stok

            $namaBarang = $request->nama_barang;
            $vendor = $request->vendor;
            $jumlah = $request->jumlah;
            $cari = Total_Stoks::all()->where('nama_barang',$namaBarang)->where('vendor',$vendor);

            if (!is_null($cari)) {
                Total_Stoks::create([
                'nama_barang' => $request->nama_barang,
                'vendor' => $request->vendor,
                'jumlah' => $request->jumlah
                ]);
                
                Stok_Masuks::create([
                'nama_barang' => $request->nama_barang,
                'vendor' => $request->vendor,
                'jumlah' => $request->jumlah
                ]);
            }
            else {                
                $update = Total_Stoks::where('nama_barang',$namaBarang)->where('vendor',$vendor)->first();
                $update->jumlah += $jumlah;
                $update->save();
            }
            

            // 3. Dan akan di pindahkan ke halaman /stok_masuk
            return redirect()->back()->with('berhasil','Data Berhasil ditambahkan!');
        }
        else {
            // 4.Jika validasi gagal, maka akan tetap di halaman /stok_masuk
            return redirect()->back()
            ->withErrors($validasi)
            ->withInput();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Proses Update Data Stok Masuk
        // 1. Validasi pada form input
        $validasi = $request->validate([
            'nama_barang' => 'min:3',
            'vendor' => 'min:3',
            'jumlah' => 'numeric'
        ]);

        if($validasi){
            // 2.Jika validasi sudah di lewati, maka masuk ke tabel stok_masuk dan total_stok
            $update = Stok_Masuks::find($id);
            $update->nama_barang = $request->nama_barang;
            $update->vendor = $request->vendor;
            $update->jumlah = $request->jumlah;
            $update->save();

            $updateTotalStok = Total_Stoks::find($id);
            $updateTotalStok->nama_barang = $request->nama_barang;
            $updateTotalStok->vendor = $request->vendor;
            $updateTotalStok->jumlah = $request->jumlah;
            $updateTotalStok->save();
            // 3. Dan akan di pindahkan ke halaman /stok_masuk
            return redirect()->back()->with('ubah','Data Berhasil Diubah!');
        }
        else {
            // 4.Jika validasi gagal, maka akan tetap di halaman /stok_masuk
            return redirect()->back()
            ->withErrors($validasi)
            ->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Menghapus data Stok Masuk per ID
        $hapus = Stok_Masuks::find($id);
        $hapus->delete();

        $TotalStok = Total_Stoks::find($id);
        $TotalStok->delete();

        // Setelah itu pindah ke halaman awal
        return redirect()->back()->with('hapus','Data Berhasil dihapus!');
    }
}
