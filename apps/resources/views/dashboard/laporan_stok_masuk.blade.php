@extends('../template/template')

@section('judul','Laporan Stok Masuk')

@section('sub-judul','Laporan Stok Masuk')

@section('konten')

<div class="card">
	<div class="card-body">
		<form method="get" action="{{ url('/laporan-stok-masuk')}}">
			<div class="row">
				<div class="col">	
					<label>Mulai Tanggal :</label>
					<input type="date" name="mulai" class="form-control">
				</div>
				<div class="col">
					<label>Sampai Tanggal :</label>
					<input type="date" name="akhir" class="form-control">
				</div>
				<div class="col">
					<input type="submit" name="cari" value="Cari" class="btn btn-primary" style="margin-top: 8%;">
					<input type="submit" name="cetak" value="Cetak PDF" class="btn btn-warning" style="margin-top: 8%;">
				</div>
			</div>
		</form>
		<br>
		
	</div>
</div>
<br>
<div class="card">
	<div class="card-body">
		<br><br>
		<center>
			<h2>Laporan Stok Masuk</h2>
		</center>
		<div class="table-responsive">
			<table class="table table-bordered">
				<thead>
					<th width="10">No</th>
					<th>Nama Barang</th>
					<th>Vendor</th>
					<th>Tanggal</th>
					<th>Jumlah</th>
				</thead>
				@foreach($stok_masuk as $sm)
				<tbody>
					<td>{{ $nomer++}} </td>
					<td>{{ $sm->nama_barang}} </td>
					<td>{{ $sm->vendor}} </td>
					<td>{{ $sm->created_at}} </td>
					<td>{{ $sm->jumlah}} </td>
				</tbody>
				@endforeach
				<tr>
					<th colspan="4">
						<center>Total</center>
					</th>
					<th>{{ $total}} </th>
				</tr>
			</table>
		</div>
	</div>
</div>
@endsection