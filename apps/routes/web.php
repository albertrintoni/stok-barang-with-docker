<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Halaman Login
Route::get('/login','Login@index')->name('login');
// Proses Login
Route::post('/login','Login@proses');
// Logout
Route::get('/logout','Login@logout');

Route::group(['middleware' => 'auth'], function(){
	// Halaman Dashboard
	Route::get('/dashboard','Dashboard@index');
	// Halaman Barang
	Route::get('/barang','Barang@index');
	// Proses Masukan Data Barang
	Route::post('/barang','Barang@store');
	// Hapus Barang per ID
	Route::get('/barang/hapus/{id}','Barang@destroy');
	// Meng-edit Barang per ID
	Route::put('/barang/update/{id}','Barang@update');

	// Halaman Vendor
	Route::get('/vendor','Vendor@index');
	// Proses Input Data Vendor
	Route::post('/vendor','Vendor@store');
	// Hapus Vendor per ID
	Route::get('/vendor/hapus/{id}','Vendor@destroy');
	// Meng-edit Vendor per ID
	Route::put('/vendor/update/{id}','Vendor@update');

	// Halaman Stok Masuk
	Route::get('/stok_masuk','Stok_Masuk@index');
	// Proses Input Data Stok Masuk
	Route::post('/stok_masuk','Stok_Masuk@store');
	// Hapus Stok Masuk per ID
	Route::get('/stok_masuk/hapus/{id}','Stok_Masuk@destroy');
	// Meng-edit Stok Masuk per ID
	Route::put('/stok_masuk/update/{id}','Stok_Masuk@update');

	// Halaman Stok Keluar
	Route::get('/stok_keluar','Stok_Keluar@index');
	// Proses Input Data Stok Keluar
	Route::post('/stok_keluar','Stok_Keluar@store');
	// Hapus Stok Keluar per ID
	Route::get('/stok_keluar/hapus/{id}','Stok_Keluar@destroy');
	// Meng-edit Stok Keluar per ID
	Route::put('/stok_keluar/update/{id}','Stok_Keluar@update');

	// Halaman Total Stok
	Route::get('/total_stok','Total_Stok@index');

	// Halaman dan Proses Laporan Stok Masuk
	Route::get('/laporan-stok-masuk','Laporan_Stok@stok_masuk');
	// Halaman dan Proses Laporan Stok Keluar
	Route::get('/laporan-stok-keluar','Laporan_Stok@stok_keluar');
	// Halaman dan Proses Laporan Total Stok
	Route::get('/laporan-total-stok','Laporan_Stok@total_stok');
});