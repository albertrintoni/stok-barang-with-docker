<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Barangs extends Model
{
    // Mengambil tabel Barang
    protected $table = 'barang';

    protected $fillable = ['nama_barang'];
}
