@extends('../template/template')

@section('judul','Barang')

@section('sub-judul','Barang')

@section('konten')
<!-- DataTales Example -->
<div class="card shadow mb-4">
		<div class="card-header py-3">
			<div class="row">
				<div class="col">
		  			<h6 class="m-0 font-weight-bold text-primary" style="padding-top: 5px;">Tabel Barang</h6>
				</div>
				<div class="col">			
		 			<button class="btn btn-primary btn-sm float-right" data-toggle="modal" data-target="#tambahBarang">Tambah Barang  
		 				<i class="fa fa-plus"></i>
		 			</button>
				</div>
			</div>
		</div>
		@if ($errors->any())
		    <div class="container">
		        <div class="alert alert-danger">
		            <ul>
		                @foreach ($errors->all() as $error)
		                    <li>{{ $error }}</li>
		                @endforeach
		            </ul>
		        </div>
		    </div>
		@endif
		<div class="card-body">
		  <div class="table-responsive">
		    <table class="table table-bordered" width="100%" cellspacing="0">
		      <thead>
		        <tr>
		          <th>No.</th>
		          <th>Nama Barang</th>
		          <th>Opsi</th>
		        </tr>
		      </thead>
		      <tbody>
		      	@foreach($barang as $b)
		        <tr>
		          <td>{{ $nomer++ }} </td>
		          <td>{{ $b->nama_barang}} </td>
		          <td>
		          	<a href="{{ url('/barang/hapus')}}/{{$b->id}}" class="text-danger">
		          		<i class="fa fa-trash"></i>
		          		Hapus
		          	</a>
		          	<button data-target="#show-{{ $b->id}}" data-toggle="modal" style="background: transparent; border: none;" class="text-primary">
		          		<i class="fa fa-pen"></i>
		          		Edit
		          	</button>
		          </td>
		        </tr>
		        @endforeach
		      </tbody>
		    </table>
		  </div>
		</div>
</div>

<!-- Modal Tambah Barang-->
<div class="modal fade" id="tambahBarang" tabindex="-1" role="dialog" aria-labelledby="tambahBarang" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Tambah Barang</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
	    <form method="post" action="{{ url('/barang')}}">
	    	{{ csrf_field()}}
	    	<label>Nama Barang : </label>
	    	<input type="text" name="nama_barang" class="form-control">  
	  </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
	    </form>
    </div>
  </div>
</div>
<!-- Modal Edit Barang-->
@foreach ($barang as $b)
<div class="modal fade" id="show-{{$b->id}}" tabindex="-1" role="dialog" aria-labelledby="tampilBarang" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Detail Barang</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form method="post" action="{{ url('/barang/update')}}/{{ $b->id }}">
	      <div class="modal-body">
		    	{{ csrf_field()}}
		    	{{ method_field('PUT')}}
		    	<label>Nama Barang : </label>
		    	<input type="text" name="nama_barang" class="form-control" value="{{ $b->nama_barang}}">  
		  </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	        <button class="btn btn-primary">Save changes</button>
	      </div>
	  </form>
    </div>
  </div>
</div>
@endforeach
@endsection