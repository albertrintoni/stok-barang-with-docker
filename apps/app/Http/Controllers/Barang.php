<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Barangs;
use App\Total_Stoks;

class Barang extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Halaman Barang

        $nomer = 1;
        $barang = Barangs::all(); // Mengambil Data Barang
        // Stok Sisa 1 Barang
        $sisaStok = Total_Stoks::where('jumlah',1)->sum('jumlah');
        // Deskripsi Sisa Stok 1 Barang
        $deskripsiSisaStok = Total_Stoks::all()->where('jumlah',1);

        return view('dashboard.barang',[
            'nomer' => $nomer,
            'barang' => $barang,
            'sisaStok' => $sisaStok,
            'deskripsiSisaStok' => $deskripsiSisaStok
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Proses Input Data Barang
        // 1. Validasi pada form input
        $validasi = $request->validate([
            'nama_barang' => 'required|min:3'
        ]);

        if($validasi){
            // 2.Jika validasi sudah di lewati, maka masuk ke tabel barang
            Barangs::create(['nama_barang' => $request->nama_barang]);
            // 3. Dan akan di pindahkan ke halaman /barang
            return redirect()->back();
        }
        else {
            // 4.Jika validasi gagal, maka akan tetap di halaman /barang
            return redirect()->back()
            ->withErrors($validasi)
            ->withInput();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Proses Update Data Barang
        // 1. Validasi pada form input
        $validasi = $request->validate([
            'nama_barang' => 'required|min:3'
        ]);

        if($validasi){
            // 2.Jika validasi sudah di lewati, maka akan memperbarui data sebelumnya ke tabel barang
            $update = Barangs::find($id);
            $update->nama_barang = $request->nama_barang;
            $update->save();
            // 3.Maka akan pindah ke halaman awal tambah barang
            return redirect()->back();
        }
        else {
            // 4.Jika gagal, maka tetap di halaman awal /barang
            return redirect()->back()
            ->withErrors($validasi)
            ->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Proses Hapus Barang per ID
        $hapus = Barangs::find($id);
        $hapus->delete();
        // Setelah itu pindah halaman ke awal
        return redirect()->back();
    }
}
