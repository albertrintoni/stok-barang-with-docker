@extends('../template/template')

@section('judul','Total Stok')

@section('sub-judul','Total Stok')

@section('konten')
<!-- DataTales Example -->
<div class="card shadow mb-4">
		<div class="card-header py-3">
			<div class="row">
				<div class="col">
		  			<h6 class="m-0 font-weight-bold text-primary" style="padding-top: 5px;">Tabel Stok Masuk</h6>
				</div>
				<div class="col">	
					<a href="/stok_masuk" class="btn btn-primary btn-sm float-right">Tambah Stok Masuk <i class="fa fa-plus"></i></a>
				</div>
			</div>
		</div>
		<div class="card-body">
		  <div class="table-responsive">
		    <table class="table table-bordered" width="100%" cellspacing="0">
		      <thead>
		        <tr>
		          <th>No.</th>
		          <th>Nama Barang</th>
		          <th>Vendor</th>
		          <th width="10">Jumlah</th>
		          <th>Tanggal Masuk</th>
		          <th>Tanggal Update Data</th>
		        </tr>
		      </thead>
		      <tbody>
		      	@foreach($totalStok as $ts)
		        <tr>
		          <td>{{ $nomer++ }} </td>
		          <td>{{ $ts->nama_barang}}</td>
		          <td>{{ $ts->vendor}}</td>
		          @if ( $ts->jumlah == 1)
		          	<td class="alert alert-danger">
		          		<center>
		          		{{ $ts->jumlah}}
		          		</center>
		          	</td>
		          @else
		          	<td>
		          		<center>
		          		{{ $ts->jumlah }} 
		          		</center>
		          	</td>
		          @endif
		          <td>{{ $ts->created_at}}</td>
		          <td>{{ $ts->updated_at}} </td>
		        </tr>
		        @endforeach
		      </tbody>
		    </table>
		  </div>
		</div>
</div>
@endsection
